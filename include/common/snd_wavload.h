#ifndef SND_WAVLOAD_H
#define SND_WAVLOAD_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\psp\snd_wavload.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\psp
 * Created Date: Thursday, November 21st 2019, 2:29:12 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"

#define AL_ALEXT_PROTOTYPES 1
#define ALC_EXT_EFX 1
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

ALboolean LoadWAVFile(const char *filename, ALenum *format, ALvoid **data, ALsizei *size, ALsizei *freq);

#endif /* SND_WAVLOAD_H */
