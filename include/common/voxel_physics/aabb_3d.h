#ifndef AABB_3D_H
#define AABB_3D_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\voxel_physics\aabb_3d.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\voxel_physics
 * Created Date: Wednesday, July 10th 2019, 2:11:02 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"
#include <cglm/cglm.h>

typedef struct AABB
{
    vec3 base;
    vec3 vec;
    vec3 max;
    //float length;
} AABB;

AABB AABB_Create(vec3 pos, vec3 vec);
AABB *AABB_Translate(AABB *aabb, vec3 by);
bool AABB_Intersects(AABB *first, AABB *second);

#endif /* AABB_3D_H */
