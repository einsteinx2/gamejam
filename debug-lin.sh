#!/bin/sh

cd build_lin && valgrind --tool=memcheck --leak-check=full --leak-resolution=high --num-callers=20 --track-origins=yes --suppressions=valgrind_ignore_lib.supp ./gamejam.elf
