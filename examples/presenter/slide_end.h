/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter\slide_end.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\presenter
 * Created Date: Wednesday, December 4th 2019, 8:03:15 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef SLIDE_END_H
#define SLIDE_END_H

#include <common.h>

extern scene sl_end;

#endif /* SLIDE_END_H */
