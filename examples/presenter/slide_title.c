#include "slide_title.h"

#include "base.h"

WINDOW_TITLE("Presenter", WINDOWED);

static void SL_Title_Init(void);
static void SL_Title_Exit(void);
static void SL_Title_Update(float time);
static void SL_Title_Render2D(float time);

STARTUP_SCENE(&SL_Title_Init, &SL_Title_Exit, &SL_Title_Render2D, NULL, &SL_Title_Update, SCENE_BLOCK);
//SCENE(slide_title, &SL_Title_Init, &SL_Title_Exit, &SL_Title_Render2D, NULL, &SL_Title_Update, SCENE_BLOCK);

static tx_image *sl_title;

static void SL_Title_Init(void)
{
    sl_title = IMG_load("sl_title.png");
    RNDR_CreateTextureFromImage(sl_title);

    /* Intentionally load but dont free */
    IMG_load("sl_internal.png");
}

static void SL_Title_Exit(void)
{
    resource_object_remove(sl_title->crc);
}

static void SL_Title_Update(float time)
{
    (void)time;
    if (INPT_ButtonEx(BTN_A, BTN_PRESS))
    {
        SCN_ChangeTo(sl_Message1);
    }
}

static void SL_Title_Render2D(float time)
{
    (void)time;

    UI_DrawPicDimensions(&rect_pos_fullscreen, sl_title);
    UI_TextSize(28);
    UI_DrawStringCentered(320, 480 - 128, "Jump IT! Postmortem");
    UI_TextSize(14);
    UI_DrawStringCentered(320, 480 - 86, "Hayden Kowalchuk");
}