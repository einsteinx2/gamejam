/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "intro_scene.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

WINDOW_TITLE("Jump IT!", WINDOWED);

static void Intro_Init(void);
static void Intro_Exit(void);
static void Intro_Update(float time);
static void Intro_Render2D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Intro_Init, &Intro_Exit, &Intro_Render2D, NULL, &Intro_Update, SCENE_BLOCK);
SCENE(scene_intro, &Intro_Init, &Intro_Exit, &Intro_Render2D, NULL, &Intro_Update, SCENE_BLOCK);

static tx_image *logo1, *logo2;

static void Intro_Init(void)
{
    logo1 = IMG_load("ndc_logo.png");
    RNDR_CreateTextureFromImage(logo1);
    logo2 = IMG_load("imr_logo.png");
    RNDR_CreateTextureFromImage(logo2);
    time_waiting = 0.0f;
    current_intro = 1;
}

static void Intro_Exit(void)
{
    resource_object_remove(logo1->crc);
    resource_object_remove(logo2->crc);
}

static void Intro_Update(float time)
{

    time_waiting += time;
    if (time_waiting >= 3.0f)
    {
        time_waiting = 0.0f;
        current_intro++;
    }
    if (current_intro == 3)
    {
        SCN_ChangeTo(main_title);
    }
}

static void Intro_Render2D(float time)
{
    (void)time;
    UI_TextSize(24);

    /* Background Teal */
    UI_DrawFill(&rect_pos_fullscreen, 173, 255, 255);

    if (time_waiting > 1.0f)
        UI_TextColorEx(1.0f, 1.0f, 1.0f, (1.0f - ((time_waiting - 1) / 2.0f)));

    switch (current_intro)
    {
    case 1:
        UI_DrawTransPicCentered(640 / 2, 480 / 2, logo1);
        UI_DrawStringCentered(640 / 2, 400, "Special Thanks to:");
        UI_DrawStringCentered(640 / 2, 430, "all DC Devs");
        break;
    case 2:
        UI_DrawTransPicCentered(640 / 2, 480 / 2, logo2);
        UI_DrawStringCentered(640 / 2, 400, "Thank you So much");
        UI_DrawStringCentered(640 / 2, 430, "Ian!");
        break;
    }
}
