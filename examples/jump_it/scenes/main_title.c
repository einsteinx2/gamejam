/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\level_select.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Sunday, November 17th 2019, 10:11:21 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "main_title.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

static unsigned char selected_index;
static float time_menu;
static float time_show;

static bool _fade_in = false;
static const char *headers[4] = {"Play", "Credits", "Data", "Quit"};

static void Title_Init(void);
static void Title_Destroy(void);
static void Title_Update(float time);
static void Title_Render2D(float time);

/* Registers the Title scene and populates the struct */
SCENE(main_title, &Title_Init, &Title_Destroy, &Title_Render2D, NULL, &Title_Update, SCENE_BLOCK);
//STARTUP_SCENE(&Title_Init, NULL, &Title_Render2D, NULL, &Title_Update, SCENE_BLOCK);

/* Scene Assets */
static tx_image *ui_texture;
static tx_image *logos;

static sprite winner;
sprite logo_dreamon, logo_neodc;

sprite square, square_trans;
static sprite circle, circle_trans;
sprite dc_controller, dc_vmu;
static sprite title_logo;

/* Sets our default values on the Title scene */
static void Title_Init(void)
{
    selected_index = 0;
    time_menu = 0;
    time_show = 0;
    Highscore_Init();

    /* Load Assets */
    ui_texture = IMG_load("ui_stuff.png");
    RNDR_CreateTextureFromImage(ui_texture);
    square = IMG_create_sprite_alt(ui_texture, 70, 2, 32, 32);
    square_trans = IMG_create_sprite_alt(ui_texture, 104, 2, 32, 32);
    circle = IMG_create_sprite_alt(ui_texture, 2, 2, 32, 32);
    circle_trans = IMG_create_sprite_alt(ui_texture, 36, 2, 32, 32);
    title_logo = IMG_create_sprite(ui_texture, 0, 114, 255, 255);

    /*dreamcast specific */
    dc_controller = IMG_create_sprite_alt(ui_texture, 0, 34, 40, 46);
    dc_vmu = IMG_create_sprite_alt(ui_texture, 42, 34, 30, 46);

    logos = IMG_load("logo.png");
    RNDR_CreateTextureFromImage(logos);
    logo_dreamon = IMG_create_sprite(logos, 0, 0, 256, 34);
    logo_neodc = IMG_create_sprite(logos, 0, 34, 156, 58);
    winner = IMG_create_sprite(logos, 0, 60, 220, 172);
}

static void Title_Destroy(void)
{

}

/* Check input and move selection Indicator */
static void Title_Update(float time)
{
    float now = Sys_FloatTime();

    /* Handle input every so often */
    if (now - time_menu > 0.12f)
    {
        extern void SND_Land(void);

        dpad_t dpad = INPT_DPAD() & 15;
        switch (dpad)
        {
        case DPAD_UP:
            if (selected_index != 0)
                selected_index--;
            SND_Land();
            time_menu = now;
            break;
        case DPAD_DOWN:
            if (selected_index != 3)
                selected_index++;

            SND_Land();
            time_menu = now;
            break;
        }
    }

    /* Execute Action */
    if (INPT_ButtonEx(BTN_A, BTN_PRESS))
    {
        switch (selected_index)
        {
        case 0:
            SCN_ChangeTo(level_select);
            break;
        case 1:
            SCN_ChangeTo(scene_credits);
            break;
        case 2:
            SCN_ChangeTo(vmu_mgmt);
            break;
        case 3:
            Sys_Quit();
            break;
        }
    }

    time_show -= time;
}

/* Draws the 2D menu, time param unused */
static void Title_Render2D(float time)
{
    (void)time;
    static float _next = 0;
    float now = Sys_FloatTime();

    if (now > _next)
    {
        _next = now + 0.5f;
        _fade_in = !_fade_in;
    }

    /* Background Teal */
    UI_DrawFill(&rect_pos_fullscreen, 173, 255, 255);

    dimen_RECT pos = {.x = 100, .y = 24, .w = 440, .h = 244};
    UI_DrawTransSprite(&pos, 1.0f, &title_logo);

    /* Menu Options */
    const int image_size = 32;
    const int spacing_h = 24;

    /* Dont show Quit on console, start lower on screen */
    const int start_x = 320;
#if defined(__arch_dreamcast) || defined(PSP)
    const int start_y = 340;
#else
    const int start_y = 300;
#endif

    UI_TextSize(16);
    UI_TextColorEx(1, 1, 1, 0.5f);
    UI_DrawString(2, 460, "Hayden Kowalchuk / mrneo240 '19");

    UI_TextSize(32);

/* Dont show Quit on console */
#if defined(__arch_dreamcast) || defined(PSP)
    for (int i = 0; i < 3; i++)
#else
    for (int i = 0; i < 4; i++)
#endif
    {
        UI_TextColor(1.0f, 1.0f, 0.0f);
        float _alpha = (_fade_in ? (0.5f - (_next - now)) / 1.0f : ((_next - now)) / 1.0f);
        if (selected_index == i)
        {
            UI_TextColorEx(0.5f, 0.5f, _alpha + 0.5f, 1.0f);
        }

        UI_DrawStringCentered(start_x, start_y - 36 + (i * (spacing_h + image_size)), headers[i]);
    }
}

