/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\spinner_scene.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Sunday, November 24th 2019, 6:35:52 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef SPINNER_SCENE_H
#define SPINNER_SCENE_H

#include <common.h>

extern scene scene_spinner;

#endif /* SPINNER_SCENE_H */
