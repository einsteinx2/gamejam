/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\level_select.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Sunday, November 17th 2019, 10:11:32 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef LEVEL_SELECT_H
#define LEVEL_SELECT_H

#include <common.h>

extern scene level_select;

#endif /* LEVEL_SELECT_H */
