/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\player.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Wednesday, October 16th 2019, 7:40:08 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "player.h"
#include "renderer.h"
#include "input.h"

#include "image_loader.h"
#include "obj_loader.h"

extern GLfloat _box_vertices3[144];
extern GLubyte _box_indices[36];

extern int boxes_2d;
extern rigidbody_2d **bodies_2d;

static bool canJump = true;
static AABB_2D aabb;

static rigidbody_2d *player;
static model_obj *player_obj;
static tx_image *player_tx;

rigidbody_2d *PLYR_2D_Get(void)
{
    return player;
}

void PLYR_2D_DisableJump(void)
{
    canJump = false;
}

void PLYR_2D_ResetJumpSimple(void)
{
    if (canJump)
    {
        return;
    }
    extern void SND_Land(void);
    SND_Land();
    canJump = true;
}

void PLYR_2D_Draw(void)
{
    if (!player)
    {
        return;
    }

    vec3 position;
    RBDY_2D_GetPosition_OGL(player, position);

    if (player_obj->num_faces > 0)
    {
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
        glDisableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glBindTexture(GL_TEXTURE_2D, player_obj->texture);

        glPushMatrix();
        glVertexPointer(3, GL_FLOAT, 5 * sizeof(float), player_obj->tris);
        glTexCoordPointer(2, GL_FLOAT, 5 * sizeof(float), ((float *)player_obj->tris) + 3);
        float normalizeAmount = MIN(1 / (player_obj->max[0] - player_obj->min[0]), 1 / (player_obj->max[2] - player_obj->min[2]));

        /*
        glTranslatef(0.5f, (player_obj->max[2] + player_obj->min[2]) / 2, 0);
        glTranslatef(position[0], position[1] - 1, position[2]);
        */
        glTranslatef(0.5f + position[0], (player_obj->max[2] + player_obj->min[2]) / 2 + position[1] - 1, position[2]);
        glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
        glDrawArrays(GL_TRIANGLES, 0, player_obj->num_tris);
        glPopMatrix();
    }
    else
    {
        glEnableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisable(GL_TEXTURE_2D);
        glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3 + 3);
        glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3);
        glDisable(GL_TEXTURE_2D);

        glPushMatrix();
        float width = RBDY_2D_GetWidth(player);
        float height = RBDY_2D_GetHeight(player);

        glTranslatef(position[0], position[1] - 1, position[2]);
        glScalef(width / 25.0f, height / 25.0f, 1);

        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
        glPopMatrix();
    }
}

#if 0
static void PLYR_2D_ResetJump(__attribute__((unused)) /*rigidbody *body*/ void *body, vec3 impacts)
{
    canJump = impacts[1] > 0.0f;
}
#endif

static int assets_loaded = 0;

void PLYR_2D_Create()
{
    vec2 pos = /*{100 + ((0+1) * 75), 100 + (-100 * 0) - (1 * 50)};*/ {0, 100};
    vec2 size = {25, 50};
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    player = bodies_2d[boxes_2d++] = PHYS_2D_AddRigidbodyEx(&aabb, 200, 0.95f, 0.75f, 1);
    player->layer = 3;

    if (!assets_loaded)
    {
        /* Attempt to load a model */
        player_obj = OBJ_load("basicCharacter.obj");
        assets_loaded = 1;
    }

    player_tx = IMG_load("skin_man.png");
    if (resource_object_count(player_tx->crc) == 1)
    {
        /* Ensure we never Free */
        IMG_load("skin_man.png");
    }
    RNDR_CreateTextureFromImage(player_tx);
    player_obj->texture = player_tx->id;

    PLYR_2D_DisableJump();
}

void PLYR_2D_Destroy()
{
    resource_object_remove(player_tx->crc);
    /* We dont actually destroy anything now, we keep after loaded */
    /*
    OBJ_destroy(&player_obj);
    IMG_destroy(&player_tx);
    */
}

static float lastUpdate = 0.0f;
void PLYR_2D_Update()
{
    if (!player)
    {
        PLYR_2D_DisableJump();
        return;
    }

    vec2 zdirection;
    vec2 speed;
    vec2 temp;
    glm_vec2_copy(GLM_VEC2_ZERO, temp);
    //glm_vec2_scale(GLM_VEC2_ONE, 20.0f, speed); /* Old Speed */
    glm_vec2_scale(GLM_VEC2_ONE, 15.0f, speed); /* New Speed */

    float now = Sys_FloatTime();
    if (now - lastUpdate > 0.25f)
    {

        if (INPT_Button(BTN_A))
        {
            if (canJump)
            {
                RBDY_2D_ApplyImpulse(player, (vec2){0, -300});
                //printf("_force: %3.2f, fcs: %3.2f, jump: %3.2f \n", player->_force[1], player->forces[1], (player->_force[1] + player->_impulses[1]));
                if ((player->_force[1] + player->_impulses[1]) > -280.0f)
                {
                    player->_force[1] = -300.0f;
                }
                extern void SND_Jump(void);
                SND_Jump();
                canJump = false;
            }
            lastUpdate = now;
        }
    }

    if (INPT_DPADDirection(DPAD_LEFT_HELD))
    {

        glm_vec2_scale((vec2){1.0f, 0.0f}, -1, zdirection);
        glm_vec2_muladd(zdirection, GLM_VEC2_ONE, temp);
    }

    if (INPT_DPADDirection(DPAD_RIGHT_HELD))
    {
        glm_vec2_muladd((vec2){1.0f, 0.0f}, GLM_VEC2_ONE, temp);
    }
    glm_vec2_normalize(temp);
    glm_vec2_mul(temp, speed, temp);
    RBDY_2D_ApplyImpulse(player, temp);
    if (player->shape.min.y > 900)
    {
        RBDY_2D_SetPosition(player, (vec2){0, 100});
    }
}
