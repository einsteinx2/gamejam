TARGET_LIB ?= libgamejam
PLATFORM ?= dreamcast

BUILD_DIR := build/$(PLATFORM)
SRC_DIRS := src
ROOT_DIR := .
INCLUDE_DIR := $(ROOT_DIR)/include
DEP_DIR := $(ROOT_DIR)/deps
LIB_DIR := $(ROOT_DIR)/lib

include $(ROOT_DIR)/Makefile.detect

REL_DIR = release
REL_LIB = lib/$(PLATFORM)/$(TARGET_LIB).a
REL_OBJS = $(SRCS:%=$(BUILD_DIR)/$(REL_DIR)/%.o)

DBG_DIR = debug
DBG_LIB = lib/$(PLATFORM)/$(TARGET_LIB)d.a
DBG_OBJS = $(SRCS:%=$(BUILD_DIR)/$(DBG_DIR)/%.o)

IGNORE_FOLDERS = psp windows linux null
FILTER_PLATFORMS = $(addprefix $(SRC_DIRS)/, $(addsuffix /%, $(IGNORE_FOLDERS)))

INC_DIRS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -type d)) $(shell find $(INCLUDE_DIR) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS)) 

SRCS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -name "*.cpp" -or -name "*.c" -or -name "*.s"))
DEPS := $(REL_OBJS:.o=.d) $(DBG_OBJS:.o=.d)

#Needed because gcc 4.7.3 is still used in places
GCCVERSIONGTEQ5 := $(shell expr `sh-elf-gcc -dumpversion | cut -f1 -d.` \>= 5)
EXTRA_CFLAGS = 

ifeq "$(GCCVERSIONGTEQ5)" "1"
	EXTRA_CFLAGS += -fdiagnostics-color
endif

BASE_CFLAGS = -DBUILD_LIBGL -Wall -Wextra -Wshadow -Wstack-protector -Wno-strict-prototypes -Wformat=0 -Wwrite-strings -std=gnu11 -fsingle-precision-constant -mpretend-cmove $(EXTRA_CFLAGS)
RELEASE_CFLAGS = $(BASE_CFLAGS) -DNDEBUG -DQUIET -g0 -Os -s -ffast-math -funsafe-math-optimizations -fomit-frame-pointer
DEBUG_CFLAGS = $(BASE_CFLAGS) -DDEBUG -g -O0 -fno-inline -fno-omit-frame-pointer

CC = sh-elf-gcc
AS = sh-elf-as -little
AR = sh-elf-ar

LIB_GLDC = $(DEP_DIR)/libgl/libGLdc.a 
LIBS = $(LIB_GLDC) $(DEP_DIR)/libcrayonvmu/lib/libcrayon_vmu.a
#deps/aldc/libAL.a
INCS = $(INC_FLAGS) -iquote $(SRC_DIRS)/common -isystem $(DEP_DIR) -I$(DEP_DIR)/stb/include -I$(DEP_DIR)/libgl/include -I$(DEP_DIR)/minilzo -I$(DEP_DIR)/cglm/include -I$(DEP_DIR)/libcrayonvmu/include -isystem deps/openal-dc/include 

# Default builds release both
all:  release debug

release: $(REL_LIB)
debug: $(DBG_LIB)

$(REL_LIB): $(LIB_GLDC) $(REL_OBJS)
	@echo -e "\n+ $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(AR) crus $@ $(REL_OBJS)

$(DBG_LIB): $(LIB_GLDC) $(DBG_OBJS)
	@echo -e "\n+ $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(AR) crus $@ $(DBG_OBJS)

$(BUILD_DIR)/$(REL_DIR)/%.c.o: %.c $(INCLUDE_DIR)/common/common.h.gch
	@echo  "> $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(RELEASE_CFLAGS) $(INCS) $(KOS_CFLAGS) $(LDFLAGS) -c -MM -MT $@ -MF $(patsubst %.o,%.d,$@) $<
	@$(CC) $(CFLAGS) $(KOS_CFLAGS) $(RELEASE_CFLAGS) $(INCS) $(LDFLAGS) -c $< -o $@

$(BUILD_DIR)/$(DBG_DIR)/%.c.o: %.c $(INCLUDE_DIR)/common/common.h.gch
	@echo  "> $@"
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(DEBUG_CFLAGS) $(INCS) $(KOS_CFLAGS) $(LDFLAGS) -c -MM -MT $@ -MF $(patsubst %.o,%.d,$@) $<
	@$(CC) $(CFLAGS) $(KOS_CFLAGS) $(DEBUG_CFLAGS) $(INCS) $(LDFLAGS) -c $< -o $@

$(INCLUDE_DIR)/common/common.h.gch:
	@$(RM) $@
	@echo  "% $@"
	@$(CC) $(CFLAGS) $(INCS) $(KOS_CFLAGS) $(LDFLAGS) -x c-header -c $(basename $@) -o $@

$(LIB_GLDC):
	$(MAKE) -C deps/libgl

examples: lib/$(PLATFORM)/$(TARGET_LIB)
	make -C examples

distclean: clean
	$(RM) -f lib/$(PLATFORM)/*

clean:
	$(RM) -rf $(BUILD_DIR) $(INCLUDE_DIR)/common/common.h.gch

-include $(DEPS)

.PHONY: clean
.PHONY: distclean
.PHONY: examples
