/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\scene.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:34:47 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "common.h"

struct StackNode *scene_root;
scene scene_current;

scene *SCN_Push(scene next)
{
    push(&scene_root, next);
    return &scene_root->data;
}

scene *SCN_ChangeTo(scene next)
{
    push(&scene_root, next);
    scene_current = pop(&scene_root);
    return &scene_current;
}

scene *SCN_Pop(void)
{
    /* Call Exit */
    if (scene_current.exit)
        (*scene_current.exit)();
        
    scene_current = pop(&scene_root);
    return &scene_current;
}