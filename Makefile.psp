_TARGET_LIB ?= libgamejam
PLATFORM ?= psp

BUILD_DIR := build/$(PLATFORM)
SRC_DIRS := src
ROOT_DIR := .
INCLUDE_DIR := $(ROOT_DIR)/include
DEP_DIR := $(ROOT_DIR)/deps
LIB_DIR := $(ROOT_DIR)/lib

include Makefile.detect

REL_DIR = release
REL_LIB = lib/$(PLATFORM)/$(_TARGET_LIB).a
REL_OBJS = $(SRCS:%=$(BUILD_DIR)/$(REL_DIR)/%.o)

DBG_DIR = debug
DBG_LIB = lib/$(PLATFORM)/$(_TARGET_LIB)d.a
DBG_OBJS = $(SRCS:%=$(BUILD_DIR)/$(DBG_DIR)/%.o)

IGNORE_FOLDERS = dreamcast windows linux null
FILTER_PLATFORMS = $(addprefix $(SRC_DIRS)/,$(addsuffix /$(WILDCARD),$(IGNORE_FOLDERS))) $(addprefix $(SRC_DIRS)/,$(IGNORE_FOLDERS))

INC_DEPS = $(SRC_DIRS) $(DEP_DIR)/stb/include $(DEP_DIR)/cglm/include  $(DEP_DIR)/libpspmath/include  $(DEP_DIR)/pthreads-emb-1.0/include $(DEP_DIR)/openal-psp/include
#INC_DIRS := $(filter-out $(FILTER_PLATFORMS), $(shell find $(SRC_DIRS) -type d)) $(shell find $(INCLUDE_DIR) -type d)
INC_DIRS := $(filter-out $(FILTER_PLATFORMS), $(call PATH_MK,$(call FIND_DIRS, $(SRC_DIRS)))) $(call PATH_MK,$(call FIND_DIRS, $(INCLUDE_DIR))) 
INC_FLAGS := $(addprefix -I,$(INC_DIRS)) $(addprefix -I,$(INC_DEPS))

#SRCS := $(filter-out $(FILTER_PLATFORMS), $(shell $(FIND) $(SRC_DIRS) -name "*.c"))
SRCS := $(filter-out $(FILTER_PLATFORMS), $(call PATH_MK,$(call FIND_FILES, $(SRC_DIRS), *.c)))
DEPS := $(REL_OBJS:.o=.d) $(DBG_OBJS:.o=.d)

BASE_CFLAGS = -DPSP -Wall -Wextra -Wshadow -Wstack-protector -Wstrict-prototypes -Wwrite-strings -Wformat=0  -std=gnu11 -fsingle-precision-constant -fdiagnostics-color
RELEASE_CFLAGS = $(BASE_CFLAGS) -DNDEBUG -g -Os -ffast-math -funsafe-math-optimizations -fomit-frame-pointer
DEBUG_CFLAGS = $(BASE_CFLAGS) -DDEBUG -O0 -g -ggdb -fno-inline -fno-omit-frame-pointer

# Use this when we release as a full iso to simplify
#ISO_FLAGS = -DPSP_ISO

LIB_PSPMATH = $(DEP_DIR)/libpspmath/libpspmath.a
LIB_PSPGL = $(DEP_DIR)/pspgl/libGL.a
LIB_PTHREADS = $(DEP_DIR)/pthreads-emb-1.0/libpthread-psp.a

LIBDIR    =
LDFLAGS   = -L$(DEP_DIR)/openal-psp/lib

LIBS    = $(LIB_PSPMATH) -lopenal-soft $(LIB_PTHREADS) -lglut -lGLU -lGL -lpspaudiolib -lpspaudio -lm -lc -lpspvfpu -lpspgum -lpspgu -lpsprtc 
INCS	  = $(INC_FLAGS) -isystem $(DEP_DIR) $(DEP_INCS)

BUILD_PRX = 0
PSP_FW_VERSION = 500
TARGET = gamejam

PSPSDK   = $(shell psp-config --pspsdk-path)
include $(ROOT_DIR)/makefiles/build.mak

all: release debug $(LIB_PSPMATH) $(LIB_PSPGL)

release: $(REL_LIB)

debug: $(DBG_LIB)

$(REL_LIB): $(REL_OBJS)
	@$(call ECHO,+ $@)
	-@$(call MKDIR_P,$(dir $@))
	@$(AR) crus $@ $^

$(DBG_LIB): $(DBG_OBJS)
	@$(call ECHO,+ $@)
	-@$(call MKDIR_P,$(dir $@))
	@$(AR) crus $@ $^

$(BUILD_DIR)/$(REL_DIR)/%.c.o: %.c
	@$(call ECHO,> $@)
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(CFLAGS) $(RELEASE_CFLAGS) $(INCS) -c $< -o $@

$(BUILD_DIR)/$(DBG_DIR)/%.c.o: %.c
	@$(call ECHO,> $@)
	-@$(call MKDIR_P,$(dir $@))
	@$(CC) $(CFLAGS) $(DEBUG_CFLAGS) $(INCS) -c $< -o $@

$(LIB_PSPGL):
	@$(MAKE) -C deps/pspgl

$(LIB_PSPMATH):
	@$(MAKE) -C deps/libpspmath	

examples: lib/$(PLATFORM)/$(_TARGET_LIB)
	make -C examples

